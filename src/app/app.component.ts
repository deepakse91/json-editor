import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

interface LeafChild {
  key: string,
  value: string | number | boolean,
  children: undefined
}

interface Child {
  key: string,
  children: (Child | LeafChild)[],
  value: undefined
}

type TreeJsonI = (Child | LeafChild)[];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public editorOptions: JsonEditorOptions;
  public data: any;
  convertedTreeJson: any = [];
  normalJson: any;
  arrayDisable = ['id', 'transaction_id', 'token_value'];

  constructor() {
    this.data = {
      'name': 'Deepak',
      "headers": {
        "MESSAGE_TYPE": "MESSAGE_CONTEXT",
        "id": "7a787509-0fd1-663a-823a-1451fc8aae42",
        "timestamp": 1589540501342
      },
      "payload": {
        "transaction_id": "79C8E12CDC13",
        "workflow_id": "1",
        "destination": "8",
        "processing_code": "003000",
        "workflow_router": [
          "CREDIT_CARD, ,TermAppWorkflowRuleAuth"
        ],
        "token_value": "411510xxxxxx5102",
        "transaction_type": "AUTHORIZATION",
        "audit_info": {
          "updated_on": "2020-09-11 02:25:28",
          "created_on": "2020-09-11 02:25:28",
          "number_of_attempt": 0
        },
        "message_collection": [
          {
            "message_exchange": {
              "request": {
                "merchant_details": {
                  "merchant_code": "00000000002",
                  "merchant_category_code": "5814",
                  "device_code": "0000000000000002",
                  "merchant_currency_code": "USD",
                  "merchant_partial_auth": "N",
                  "billing_address": {
                    "postal_code": "2001"
                  },
                  "merchant_institution_code": "00000000002"
                },
                "local_address": "10.1.13.29",
                "point_of_sale": {
                  "capabilities": {
                    "terminal_output_capability": "PRINTING",
                    "posTerminalLocationIndicator": "ON_PREMISES_CARD_ACCEPTOR_FACILITY",
                    "card_holder_verification_method": "ONLINE_PIN",
                    "pos_entry_mode": "ICC",
                    "pin_capture_capability": "CHAR_4",
                    "cardholderPresenceIndicator": "CARDHOLDER_PRESENT",
                    "terminal_operator": "CARD_ACCEPTOR_OPERATED",
                    "pos_environment": "POS",
                    "card_holder_authentication_entity": "MERCHANT",
                    "card_present": true,
                    "cardCaptureCapability": "CAPTURE"
                  },
                  "function_code": "100",
                  "terminal_id": "0000000000000004"
                },
                "acquirer_institution_id": "00000000009",
                "gateway_client_address": "10-1-13-29.reno-test-headless-agent.reno-test.svc.cluster.local:60604:2020:3913f48d-13fa-46fb-839b-c895a9afd7a9",
                "card": {
                  "mask_pan_number": "542511XXXXXX3638",
                  "enc_pan_number": "NTQyNTExODg4NDc2MzYzOA==",
                  "emv_tag_reversal_map": {},
                  "card_type": "Mastercard"
                },
                "transaction": {
                  "transaction_date": "2020-09-11 02:25:28",
                  "additional_amount_map": {},
                  "transmission_date_time": "0515110136",
                  "system_trace_audit_number": "955704",
                  "transaction_amount": {
                    "amount": 89.19,
                    "amount_type": "TXN_AMOUNT",
                    "currency_minor_unit": 2,
                    "currency_iso_code": "840",
                    "currency_code": "USD"
                  },
                  "retrieval_reference_number": "119177106332",
                  "processing_code": "00"
                },
                "audit_info": {
                  "updated_on": "2020-05-15 11:01:38",
                  "created_on": "2020-05-15 11:01:38",
                  "number_of_attempt": 0
                },
                "additional_attributes": {
                  "security_related_control_information": "000000000000000000000000000000000000000000000000",
                  "isVelocityDefined": "N",
                  "reconciliation_indicator": "112"
                }
              },
              "service_type": "GATEWAY_SERVICE",
              "response": {
                "response_code": "000",
                "transaction_response": {
                  "account_balances": [],
                  "settlement_date": "Mar 24",
                  "additional_amount_map": {
                    "AUTHORISED_AMOUNT": {
                      "amount": 89.19,
                      "amount_type": "TXN_AMOUNT",
                      "currency_minor_unit": 2,
                      "currency_code": "USD"
                    }
                  },
                  "capture_date": "May 15",
                  "auth_code": "723931"
                },
                "transaction_type": "AUTHORIZATION",
                "audit_info": {
                  "updated_on": "2020-05-15 11:01:38",
                  "created_on": "2020-05-15 11:01:38",
                  "number_of_attempt": 0
                },
                "additional_attributes": {
                  "RECEIVING_INSTITUTION_IDENTIFICATION_CODE": "47"
                }
              },
              "adapter_id": "kafkaTxn"
            }
          },
          {
            "message_exchange": {
              "request": {
                "merchant_details": {
                  "merchant_code": "00000000002",
                  "merchant_category_code": "5814",
                  "device_code": "0000000000000002",
                  "merchant_currency_code": "USD",
                  "merchant_partial_auth": "N",
                  "billing_address": {
                    "postal_code": "2001"
                  },
                  "merchant_institution_code": "00000000002"
                },
                "card": {
                  "emv_tag_reversal_map": {}
                },
                "transaction": {
                  "transaction_date": "2020-05-15 11:01:36",
                  "additional_amount_map": {},
                  "transmission_date_time": "0515110136",
                  "system_trace_audit_number": "496094",
                  "transaction_amount": {
                    "amount": 85,
                    "amount_type": "TXN_AMOUNT",
                    "currency_minor_unit": 2,
                    "currency_iso_code": "840",
                    "currency_code": "USD"
                  },
                  "retrieval_reference_number": "589540496094",
                  "processing_code": "00"
                },
                "audit_info": {
                  "updated_on": "2020-05-15 11:01:38",
                  "created_on": "2020-05-15 11:01:38",
                  "number_of_attempt": 0
                },
                "additional_attributes": {}
              },
              "service_type": "AUTH_SERVICE",
              "route": "8",
              "response": {
                "response_code": "00",
                "transaction_response": {
                  "account_balances": [],
                  "settlement_date": "Mar 24",
                  "additional_amount_map": {
                    "AUTHORISED_AMOUNT": {
                      "amount": 85,
                      "amount_type": "TXN_AMOUNT",
                      "currency_minor_unit": 2,
                      "currency_code": "USD"
                    }
                  },
                  "capture_date": "May 15",
                  "auth_code": "723931"
                },
                "transaction_type": "AUTHORIZATION",
                "audit_info": {
                  "updated_on": "2020-05-15 11:01:38",
                  "created_on": "2020-05-15 11:01:38",
                  "number_of_attempt": 0
                },
                "additional_attributes": {
                  "RECEIVING_INSTITUTION_IDENTIFICATION_CODE": "47"
                }
              },
              "internal_processing_code": "APPROVED",
              "adapter_id": "base24BicISOCart"
            }
          }
        ],
        "acquirer_id": "1",
        "internal_processing_code": "APPROVED",
        "network_status": "NETWORK_SUCCESS",
        "destination_router": [
          "AUTH_SERVICE, ,TermAppRoutingRuleAuth"
        ],
        "payment_method": "CREDIT_CARD"
      }
    }

    this.convertedTreeJson = this.getNodes(this.data);
  }

  ngOnInit() {
    console.log("Tree JSON: ", this.convertedTreeJson);
  }

  getNodes(object: any) {
    return Object
      .entries(object)
      .map(([key, value]) => value && typeof value === 'object'
        ? { title: key, key, children: this.getNodes(value), expanded: true }
        : { title: key, key, value, duplicateValue: value, highlight: '', disableKey: this.arrayDisable.indexOf(key) === -1 ? false : true }
      );
  }

  valueOnChange(event: any, obj: any) {
    if (event.target.value === obj.duplicateValue) {
      obj.highlight = '';
    } else {
      obj.highlight = 'color-highlight';
    }
  }

  saveJSON() {
    let getJsonChanges = this.getJsonChangesMethod(this.convertedTreeJson);
    if (getJsonChanges) {
      this.normalJson = this.convertTreeToFlatJson(this.convertedTreeJson)
      console.log("Normal JSON: ", this.normalJson);
    } else {
      alert('Please edit the JSON first!');
    }
  }

  convertTreeToFlatJson(treeJson: TreeJsonI): any {
    let flatJson = {};

    for (let c of treeJson) {
      this.eachRecursive(c, flatJson);
    }

    return flatJson;
  }

  eachRecursive(child: Child | LeafChild, baseParent: any) {
    const isBaseParentArray: boolean = Array.isArray(baseParent);

    if (child.children === undefined) {
      //this is leaf child
      if (isBaseParentArray) {
        baseParent[parseInt(child.key)] = child.value;
      } else {
        baseParent[`${child.key}`] = child.value;
      }
      return;
    } else {
      let nestedParent: any;
      if (child.children.length == 0 || (typeof child.children[0].key === "string" && !isNaN(child.children[0].key as any))) {
        //it is an array
        nestedParent = [];
      } else {
        //it is a json object
        nestedParent = {};
      }

      if (isBaseParentArray) {
        baseParent[parseInt(child.key)] = nestedParent;
      } else {
        baseParent[`${child.key}`] = nestedParent;
      }

      for (let c of child.children) {
        this.eachRecursive(c, nestedParent);
      }
    }
  }

  // check the changes of json
  foundChanges: boolean = false;
  getJsonChangesMethod(jsonArray: any) {
    this.foundChanges = false;
    for (let item of jsonArray) {
      this.testEveryObject(item);
    }
    return this.foundChanges;
  }

  testEveryObject(item: any) {
    if (item.children === undefined) {
      if (item.value !== item.duplicateValue) {
        this.foundChanges = true;
        return;
      }
    } else {
      if (item.children && item.children.length > 0) {
        for (let child of item.children) {
          this.testEveryObject(child);
        }
      }
    }
  }

}
